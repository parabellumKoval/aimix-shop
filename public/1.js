(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/cards/ProductCard.vue?vue&type=script&lang=js&":
/*!****************************************************************************************************************************************************************************!*\
  !*** ./node_modules/babel-loader/lib??ref--4-0!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/cards/ProductCard.vue?vue&type=script&lang=js& ***!
  \****************************************************************************************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'productcard',
  data: function data() {
    return {
      item: this.dataItem,
      classes: this.dataClasses
    };
  },
  props: ['dataItem', 'dataClasses'],
  computed: {
    isActive: function isActive() {
      return this.$root.favorites.includes(this.item.id);
    }
  },
  methods: {
    addToFavorites: function addToFavorites() {
      this.$root.$emit('add-to-favorites', this.item);
    }
  },
  watch: {
    dataItem: {
      handler: function handler(value) {
        this.item = value;
      },
      deep: true
    }
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/cards/ProductCard.vue?vue&type=template&id=283810d0&":
/*!********************************************************************************************************************************************************************************************************************!*\
  !*** ./node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!./node_modules/vue-loader/lib??vue-loader-options!./resources/js/components/cards/ProductCard.vue?vue&type=template&id=283810d0& ***!
  \********************************************************************************************************************************************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "render", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("li", { staticClass: "products-item", class: _vm.classes }, [
    _c("button", {
      staticClass: "products-item__open-popup js-button",
      attrs: { "data-target": "product" },
      on: {
        click: function($event) {
          _vm.$root.productDetails = _vm.item
        }
      }
    }),
    _vm._v(" "),
    _c("div", { staticClass: "products-item__img products-item__img--else" }, [
      _c("img", {
        directives: [
          {
            name: "lazy",
            rawName: "v-lazy",
            value: _vm.item.image,
            expression: "item.image"
          }
        ],
        attrs: { alt: _vm.item.name }
      }),
      _vm._v(" "),
      _c("img", {
        directives: [
          {
            name: "lazy",
            rawName: "v-lazy",
            value: "./img/product-hover-img.png",
            expression: "'./img/product-hover-img.png'"
          }
        ],
        attrs: { alt: "title" }
      })
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "products-item__container" }, [
      _c("div", { staticClass: "products-item__main-info" }, [
        _c("p", { staticClass: "products-item__category" }, [
          _vm._v(_vm._s(_vm.item.category_name))
        ]),
        _vm._v(" "),
        _c("a", { staticClass: "products-item__link", attrs: { href: "#" } }, [
          _vm._v(_vm._s(_vm.item.name))
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "product-item__price product-price" }, [
          _vm.item.old_price
            ? _c(
                "p",
                { staticClass: "product__old-price product__old-price--item" },
                [_vm._v(_vm._s(_vm.item.old_price) + " грн")]
              )
            : _vm._e(),
          _vm._v(" "),
          _c("p", { staticClass: "product__price product__price--item" }, [
            _vm._v(_vm._s(_vm.item.price) + " грн")
          ])
        ])
      ]),
      _vm._v(" "),
      _vm._m(0)
    ]),
    _vm._v(" "),
    _c(
      "button",
      {
        staticClass: "add-to-favorite",
        class: { active: _vm.isActive },
        on: {
          click: function($event) {
            return _vm.addToFavorites()
          }
        }
      },
      [_c("span", { staticClass: "icon-add-favorite" })]
    ),
    _vm._v(" "),
    _vm.item.old_price
      ? _c("div", { staticClass: "product-noty product-noty--products-item" }, [
          _vm._v("SALE")
        ])
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "products-item__parameters" }, [
      _c("ul", { staticClass: "product-category__color-list" }, [
        _c("li", {
          staticClass: "product-category__color-item",
          staticStyle: { "background-color": "red" }
        }),
        _vm._v(" "),
        _c("li", {
          staticClass: "product-category__color-item",
          staticStyle: { "background-color": "blue" }
        }),
        _vm._v(" "),
        _c("li", {
          staticClass: "product-category__color-item",
          staticStyle: { "background-color": "blueviolet" }
        })
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./resources/js/components/cards/ProductCard.vue":
/*!*******************************************************!*\
  !*** ./resources/js/components/cards/ProductCard.vue ***!
  \*******************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ProductCard_vue_vue_type_template_id_283810d0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ProductCard.vue?vue&type=template&id=283810d0& */ "./resources/js/components/cards/ProductCard.vue?vue&type=template&id=283810d0&");
/* harmony import */ var _ProductCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ProductCard.vue?vue&type=script&lang=js& */ "./resources/js/components/cards/ProductCard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport *//* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../../node_modules/vue-loader/lib/runtime/componentNormalizer.js */ "./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__["default"])(
  _ProductCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__["default"],
  _ProductCard_vue_vue_type_template_id_283810d0___WEBPACK_IMPORTED_MODULE_0__["render"],
  _ProductCard_vue_vue_type_template_id_283810d0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "resources/js/components/cards/ProductCard.vue"
/* harmony default export */ __webpack_exports__["default"] = (component.exports);

/***/ }),

/***/ "./resources/js/components/cards/ProductCard.vue?vue&type=script&lang=js&":
/*!********************************************************************************!*\
  !*** ./resources/js/components/cards/ProductCard.vue?vue&type=script&lang=js& ***!
  \********************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/babel-loader/lib??ref--4-0!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductCard.vue?vue&type=script&lang=js& */ "./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/cards/ProductCard.vue?vue&type=script&lang=js&");
/* empty/unused harmony star reexport */ /* harmony default export */ __webpack_exports__["default"] = (_node_modules_babel_loader_lib_index_js_ref_4_0_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductCard_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__["default"]); 

/***/ }),

/***/ "./resources/js/components/cards/ProductCard.vue?vue&type=template&id=283810d0&":
/*!**************************************************************************************!*\
  !*** ./resources/js/components/cards/ProductCard.vue?vue&type=template&id=283810d0& ***!
  \**************************************************************************************/
/*! exports provided: render, staticRenderFns */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductCard_vue_vue_type_template_id_283810d0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! -!../../../../node_modules/vue-loader/lib/loaders/templateLoader.js??vue-loader-options!../../../../node_modules/vue-loader/lib??vue-loader-options!./ProductCard.vue?vue&type=template&id=283810d0& */ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/vue-loader/lib/index.js?!./resources/js/components/cards/ProductCard.vue?vue&type=template&id=283810d0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "render", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductCard_vue_vue_type_template_id_283810d0___WEBPACK_IMPORTED_MODULE_0__["render"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "staticRenderFns", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_vue_loader_lib_index_js_vue_loader_options_ProductCard_vue_vue_type_template_id_283810d0___WEBPACK_IMPORTED_MODULE_0__["staticRenderFns"]; });



/***/ })

}]);