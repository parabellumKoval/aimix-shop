<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Article extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'category_slug' => $this->category->slug,
            'content' => $this->content,
            'category_name' => $this->category->name,
            'image' => url($this->image),
            'date' => $this->date->format('d.m.Y h:i')
        ];
    }
}
