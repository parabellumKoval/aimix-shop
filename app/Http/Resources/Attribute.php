<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Attribute extends JsonResource
{
    public $preserveKeys = true;
    
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
          'id' => $this->id,
          'name' => $this->name,
          'slug' => $this->slug,
          'attribute_group_id' => $this->attribute_group_id,
          'icon' => $this->icon,
          'description' => $this->description,
          'si' => $this->si,
          'default_value' => $this->default_value,
          'values' => $this->type == 'colors'? $this->colorsValues : $this->values,
          'type' => $this->type,
          'is_important' => $this->is_important,
          'is_active' => $this->is_active,
          'in_filters' => $this->in_filters,
          'in_properties' => $this->in_properties,
          'human_value' => $this->humanValue,
        ];
    }
}
