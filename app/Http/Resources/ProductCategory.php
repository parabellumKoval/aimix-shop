<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductCategory extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'link' => $this->inner_link,
            'image' => url($this->image),
            'description' => $this->description,
            'meta_title' => $this->meta_title,
            'meta_desc' => $this->meta_desc,
            'prices' => $this->prices,
            'children' => ProductCategory::collection($this->children),
            'product_count' => $this->products->count()
        ];
    }
}
