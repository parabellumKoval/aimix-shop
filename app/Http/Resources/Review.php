<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Review extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id" => $this->id,
            "is_moderated" => $this->is_moderated,
            "type" => $this->type,
            "name" => $this->name,
            "email" => $this->email,
            "category" => $this->category,
            "product_id" => $this->product_id,
            "product_link" => $this->product ? $this->product->link : null,
            "rating" => $this->rating,
            "file" => $this->file,
            "images" => $this->images? $this->imagesArray : null,
            "text" => $this->text,
            "created_at" => $this->created_at->format('d.m.Y'),
          ];
    }
}
