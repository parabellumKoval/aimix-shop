<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Product extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'slug' => $this->slug,
            'category_id' => $this->category_id,
            'category_name' => $this->category->name,
            // 'brand_id' => $this->brand_id,
            'brand_name' => $this->brand? $this->brand->name : '',
            'price' => $this->baseModification->price,
            'old_price' => $this->baseModification->old_price,
            // 'sale_percent' => $this->salePercent,
            'is_active' => $this->is_active,
            'is_hit' => $this->is_hit,
            // 'is_new' => $this->is_new,
            // 'is_recommended' => $this->is_recommended,
            'rating' => $this->rating,
            'attrs' => $this->baseModification->getPluckedAttributesArray(),
            'extras' => $this->extras,
            'image' => url($this->image),
            'images' => $this->baseModification->images,
            'link' => $this->inner_link,
            // 'amount' => isset($this->amount)? $this->amount : 1,
            // 'code' => $this->code,
            // 'in_stock' => $this->baseModification->in_stock,
            'description' => nl2br($this->description),
            // 'discount_amount' => $this->discountAmount
            
          ];
    }
}
