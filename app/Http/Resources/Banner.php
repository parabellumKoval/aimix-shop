<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Banner extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'title' => $this->title,
            'short_desc' => $this->short_desc,
            'desc' => $this->desc,
            'button_text' => $this->button_text,
            'link' => $this->link,
            'type' => $this->type,
            'image' => url($this->image)
        ];
    }
}
