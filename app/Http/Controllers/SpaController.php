<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SpaController extends Controller
{
    public function index()
    {
        $data['app_name'] = config('app.name');

        return view('index', $data);
    }
}