<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Aimix\Shop\app\Models\Brand;

class BrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \App\Http\Resources\Brand::collection(Brand::get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Display a listing of brands for category and category's children.
     *
     * @param  int  $category_id
     * @return \Illuminate\Http\Response
     */

    public function byCategory($category_id)
    {
        $brands = Brand::select('brands.*')
                       ->distinct('brands.id')
                       ->join('products', 'products.brand_id', '=', 'brands.id')
                       ->join('product_categories', 'product_categories.id', '=', 'products.category_id')
                       ->where('product_categories.id', $category_id)
                       ->orWhere('product_categories.parent_id', $category_id)
                       ->pluck('name', 'id');
                       
        return $brands;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
