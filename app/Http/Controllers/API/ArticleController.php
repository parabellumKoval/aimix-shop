<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Backpack\NewsCRUD\app\Models\Article;
use Backpack\NewsCRUD\app\Models\Category;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \App\Http\Resources\Article::collection(Article::get());
    }

    public function paginate(Request $request)
    {
        $limit = $request->limit? $request->limit : 4;
        $sort_string = $request->sort? $request->sort : 'date_desc';
        $sort = $this->getSortArray($sort_string);

        $articles = Article::where('status', 'PUBLISHED');

        if($request->category_slug) {
            $category = Category::where('slug', $request->category_slug)->first();
            $articles = $articles->where('category_id', $category->id);
        }

        $articles = $articles->orderBy($sort['value'], $sort['dirr'])->paginate($limit);

        return \App\Http\Resources\Article::collection($articles);
    }

    private function getSortArray($sort_string){
	    preg_match_all("/([\w]+)_([\w]+)/", $sort_string, $value);
	    
	    return ['value' => $value[1][0], 'dirr' => $value[2][0]];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return new \App\Http\Resources\Article(Article::where('slug', $slug)->first());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
