<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Aimix\Shop\app\Models\Attribute;
use Aimix\Shop\app\Models\Category;

class AttributeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \App\Http\Resources\Attribute::collection(Attribute::get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return new \App\Http\Resources\Attribute(Attribute::where('slug', $slug)->first());
    }
    
    /**
     * Display a listing of attributes for category and category's children.
     *
     * @param  int  $category_id
     * @return \Illuminate\Http\Response
     */

    public function byCategory($category_id)
    {
        $attrs = Attribute::select('attributes.*')
                          ->distinct('attributes.id')
                          ->join('attribute_category', 'attribute_category.attribute_id', '=', 'attributes.id')
                          ->leftJoin('product_categories as children', 'children.id', '=', 'attribute_category.category_id')
                          ->where('attributes.in_filters', 1)
                          ->where(function($query) use ($category_id) {
                                $query->where('attribute_category.category_id', $category_id)->orWhere('children.parent_id', $category_id);
                            });

        $attrs = $attrs->get()->keyBy('id');
        return \App\Http\Resources\Attribute::collection($attrs);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
