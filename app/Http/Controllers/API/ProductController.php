<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Aimix\Shop\app\Models\Product;
use Aimix\Shop\app\Models\Attribute;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return \App\Http\Resources\Product::collection(Product::get());
    }

    public function popular()
    {
        return new \App\Http\Resources\ProductCollection(Product::hits()->paginate(4));
    }

    // get products by ids array
    public function byIds(Request $request)
    {
        $ids = $request->ids;

        return new \App\Http\Resources\ProductCollection(Product::find($ids));
    }

    // get products for catalog
    public function filtered(Request $request)
    {
        $products = Product::with(['modifications', 'category'])->active()->distinct('products.id')->select('products.*')->join('modifications', 'modifications.product_id', '=', 'products.id');
        // ->where('modifications.is_default', 0);

        if($request->category_id) {
            $products = $products->join('product_categories', 'product_categories.id', '=', 'products.category_id')
                               ->where(function($query) use ($request) {
                                   $query->where('product_categories.id', $request->category_id)
                                         ->orWhere('product_categories.parent_id', $request->category_id);
                               });
        }
        
        if($request->brands) {
            $products = $products->whereIn('products.brand_id', $request->brands);
        }
        
        if($request->price) 
        $products = $products->where('modifications.price', '>=', $request->price[0])->where('modifications.price', '<=', $request->price[1]);

        foreach($request->input('attributes') as $attr_id => $attr_value) {
            if(!count($attr_value))
              continue;
    
            $attr_name = 'attr_' . $attr_id;
    
            if(Attribute::find($attr_id)->type === 'number') {
                $products = $products->join('attribute_modification as ' . $attr_name, function($join) use ($attr_name, $attr_value, $attr_id) {
                    $join->on($attr_name . '.modification_id', '=', 'modifications.id')->where($attr_name . '.attribute_id', $attr_id)->where($attr_name . '.value', '>=', (int)$attr_value[0])->where($attr_name . '.value', '<=', (int)$attr_value[1]);
                });
            } else {
                $products = $products->join('attribute_modification as ' . $attr_name, function($join) use ($attr_value, $attr_id, $attr_name) {
                    $join->on($attr_name . '.modification_id', '=', 'modifications.id')->where($attr_name . '.attribute_id', $attr_id);
        
                    $join->where(function($query) use ($attr_value, $attr_name) {
                        foreach($attr_value as $key => $val) {
                        $whereJsonContainsFunction = $key == 0? 'whereJsonContains' : 'orWhereJsonContains';
            
                        $query->{$whereJsonContainsFunction}($attr_name . '.value', $val);
                        }
                    });
                });
            }
        }    
        
        $products = $products->paginate(config('aimix.shop.per_page'));

        return new \App\Http\Resources\ProductCollection($products);
    }

    
    // get products for catalog
    public function byCategory(Request $request)
    {
        $products = Product::select('products.*')->distinct('products.id');

        if($request->category_id) {
            $products = $products->join('product_categories', 'product_categories.id', '=', 'products.category_id')
                               ->where(function($query) use ($request) {
                                   $query->where('product_categories.id', $request->category_id)
                                         ->orWhere('product_categories.parent_id', $request->category_id);
                               });
        }

        return \App\Http\Resources\Product::collection($products->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
