<?php

namespace App\Http\Controllers\Admin;
use Backpack\NewsCRUD\app\Http\Controllers\Admin\CategoryCrudController as BaseController;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Backpack\NewsCRUD\app\Http\Requests\CategoryRequest;

class CategoryCrudController extends BaseController
{
    protected function setupCreateOperation()
    {
        CRUD::setModel("App\Models\Category");
        CRUD::setValidation(CategoryRequest::class);

        CRUD::addField([
            'name' => 'name',
            'label' => 'Name',
        ]);
        CRUD::addField([
            'name' => 'slug',
            'label' => 'Slug (URL)',
            'type' => 'text',
            'hint' => 'Will be automatically generated from your name, if left empty.',
            // 'disabled' => 'disabled'
        ]);
        CRUD::addField([
            'label' => 'Parent',
            'type' => 'select',
            'name' => 'parent_id',
            'entity' => 'parent',
            'attribute' => 'name',
        ]);
        CRUD::addField([
            'label' => 'Meta title',
            'type' => 'text',
            'name' => 'meta_title',
        ]);
        CRUD::addField([
            'label' => 'Meta description',
            'type' => 'textarea',
            'name' => 'meta_desc',
            'attributes' => [
                'rows' => 6,
            ]
        ]);
        CRUD::addField([
            'label' => 'Seo title (h2)',
            'type' => 'text',
            'name' => 'seo_title',
        ]);
        CRUD::addField([
            'label' => 'Seo text',
            'type' => 'ckeditor',
            'name' => 'seo_text'
        ]);
    }
}
