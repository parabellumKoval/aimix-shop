<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Backpack\NewsCRUD\app\Models\Article as BaseModel;

class Article extends BaseModel
{
    use HasFactory;

    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }
}
