<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Backpack\NewsCRUD\app\Models\Category as BaseModel;

class Category extends BaseModel
{
    use HasFactory;
    
    protected $fillable = ['name', 'slug', 'parent_id', 'meta_title', 'meta_desc', 'seo_title', 'seo_text'];

    public function articles()
    {
        return $this->hasMany('App\Models\Article');
    }
}
