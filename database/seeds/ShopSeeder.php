<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
use Aimix\Banner\app\Models\Banner;
use Aimix\Review\app\Models\Review;
use Aimix\Shop\app\Models\Category as ProductCategory;
use Aimix\Shop\app\Models\Product;
use Aimix\Shop\app\Models\Brand;
use Aimix\Shop\app\Models\Attribute;
use App\Models\Faq;
use App\Models\FaqCategory;
use App\User;
use Illuminate\Support\Facades\Hash;

class ShopSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('11223344'),
        ]);
        
        Category::factory()
                ->times(5)
                ->hasArticles(10)
                ->create();

        Brand::factory()
                ->times(5)
                ->create();

        Banner::factory()
              ->times(5)
              ->create();

        Review::factory()
              ->times(20)
              ->create();

        ProductCategory::factory()
                       ->times(5)
                       ->has(ProductCategory::factory()->count(5)->hasAttributes(3), 'children')
                       ->has(ProductCategory::factory()
                                            ->count(2)
                                            ->state(['is_popular' => 1])
                                            ->hasAttributes(3), 'children')
                       ->create();

        Product::factory()
               ->times(100)
               ->create();

        FaqCategory::factory()
                   ->times(5)
                   ->hasQuestions(5)
                   ->create();
    }
}
