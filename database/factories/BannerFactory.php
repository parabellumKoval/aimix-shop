<?php

namespace Database\Factories\Aimix\Banner\app\Models;

use Aimix\Banner\app\Models\Banner;
use Illuminate\Database\Eloquent\Factories\Factory;

class BannerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Banner::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence,
            'short_desc' => $this->faker->word,
            'desc' => $this->faker->sentence,
            'button_text' => 'Catalog',
            'link' => '/catalog',
            'image' => '/test.jpg'
        ];
    }
}
