<?php

namespace Database\Factories\Aimix\Shop\app\Models;

use Aimix\Shop\app\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductCategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => ucfirst($this->faker->word),
            'parent_id' => null,
            'image' => '/test.jpg',
            'description' => $this->faker->paragraph
        ];
    }
}
