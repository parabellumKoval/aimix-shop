<?php

namespace Database\Factories\Aimix\Shop\app\Models;

use Aimix\Shop\app\Models\Brand;
use Illuminate\Database\Eloquent\Factories\Factory;

class BrandFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Brand::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => ucfirst($this->faker->word),
            'logo' => '/test.jpg',
            'description' => $this->faker->paragraph
        ];
    }
}
