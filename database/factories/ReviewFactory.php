<?php

namespace Database\Factories\Aimix\Review\app\Models;

use Aimix\Review\app\Models\Review;
use Illuminate\Database\Eloquent\Factories\Factory;

class ReviewFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Review::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'text' => $this->faker->sentence,
            'file' => '/test.jpg',
            'is_moderated' => 1,
            'rating' => rand(1, 5),
            'created_at' => $this->faker->dateTimeBetween('-1 years', 'now')
        ];
    }
}
