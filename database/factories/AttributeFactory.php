<?php

namespace Database\Factories\Aimix\Shop\app\Models;

use Aimix\Shop\app\Models\Attribute;
use Illuminate\Database\Eloquent\Factories\Factory;

class AttributeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Attribute::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        switch (rand(0,4)) {
            case 0:
                $type = 'number';
                break;
                
            case 1:
            case 2:
                $type = 'checkbox';
                break;
            
            default:
                $type = 'radio';
                break;
        }
        
        $values = $type === 'number'? 
        [
            'min' => 1, 
            'max' => 100, 
            'step' => 1
        ] : 
        [
            $this->faker->word,
            $this->faker->word,
            $this->faker->word
        ];

        $default = $type === 'number'? 50 : $values[rand(0,2)];

        return [
            'name' => $this->faker->word,
            'type' => [
                'type' => $type,
                'values' => $values,
            ],
            'si' => $type === 'number'? 'кг' : null,
            'default_value' => $default,
        ];
    }
}
