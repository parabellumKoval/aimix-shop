<?php

namespace Database\Factories\Aimix\Shop\app\Models;

use Aimix\Shop\app\Models\Product;
use Aimix\Shop\app\Models\Brand;
use Aimix\Shop\app\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $price = rand(5, 30) * 100;
        $brand_id = Brand::count()? Brand::inRandomOrder()->first()->id : null;
        $category = Category::whereDoesntHave('children')->inRandomOrder()->first();
        $attrs = [];

        foreach($category->attributes as $attr) {
            $type = $attr['type'];

            switch ($type) {
                case 'number':
                    $value = rand($attr['values']->min, $attr['values']->max);
                    break;
                    
                case 'radio':
                    $value = $attr['values'][array_rand($attr['values'])];
                    break;
                
                case 'checkbox':
                    $value = [$attr['values'][array_rand($attr['values'])]];
                    break;
            }

            $attrs[$attr->id] = [
                'value' => $value
            ];
        }

        return [
            'name' => ucfirst($this->faker->word),
            'is_active' => 1,
            'is_hit' => rand(0, 9) === 0,
            'brand_id' => $brand_id,
            'category_id' => $category->id,
            'image' => '/test.jpg',
            'description' => $this->faker->paragraph,
            'mod' => [
                0 => [
                    'id' => null,
                    'price' => $price,
                    'old_price' => rand(0, 9) === 0? $price + rand(1, 5) * 100 : null,
                    'is_default' => 1,
                    'is_active' => 1,
                    'name' => 'base',
                    'attrs' => $attrs
                ]
            ]
        ];
    }
}
