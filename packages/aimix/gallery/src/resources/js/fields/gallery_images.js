require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue';

const gallery = new Vue({
  el: "#gallery_field",
  data: {
    images: images,
  },
  methods: {
    addImage: function() {
      this.images.push({});
    },
    removeImage: function(index) {
      Vue.delete(this.images, index);
    }
  },
  created: function() {
    console.log('Gallery vue created');
  }
});