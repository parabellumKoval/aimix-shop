<?php

namespace Aimix\Shop\app\Observers;

use Aimix\Shop\app\Models\Product;

class ProductObserver
{
    private $product;
    
    // public function created(Product $product){
    //   $this->product = $product;
      
    //   if(!$product->wasSaved){
    //     $product->wasSaved = true;
    //     $this->updateOrCreateModification($product->modifications_array);
    //   }
    // }
    
    public function saved(Product $product){
      $product->modifications_array = $product->modifications_array? $product->modifications_array : request()->input('mod');
      $this->product = $product;
      
      if(!$product->isModificationRelation && $product->modifications_array){
          $product->modifications_array[0]['images'] = $product->images_array;
        
          $this->updateOrCreateModification($product->modifications_array);
      }
    }
    
    public function updateOrCreateModification($modifications) {
      foreach($modifications as $modification){
        $this->product->modifications()->updateOrCreate([
          'id' => $modification['id'],
        ], $modification);
      }
    }
    
    public function deleting(Product $product) {
      foreach($product->modifications as $modification){
        $modification->attrs()->detach();
      }
      
      $product->modifications()->delete();
    }
}
