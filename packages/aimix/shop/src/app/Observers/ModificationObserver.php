<?php

namespace Aimix\Shop\app\Observers;

use Aimix\Shop\app\Models\Modification;

class ModificationObserver
{
    public function saved(Modification $modification){
      $modification->attrs()->sync($modification->attributes_array);
    }
    
}
