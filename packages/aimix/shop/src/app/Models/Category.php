<?php

namespace Aimix\Shop\app\Models;

use Illuminate\Database\Eloquent\Builder;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Category extends Model
{
    use CrudTrait;
    use Sluggable;
    use SluggableScopeHelpers;
    use HasFactory;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'product_categories';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    protected static function boot()
    {

        parent::boot();
        if(config('aimix.aimix.enable_languages')) {
            $language = session()->has('lang')? session()->get('lang'): 'ru';
            
            static::addGlobalScope('language', function (Builder $builder) use ($language) {
                $builder->where('language_abbr', $language);
            });
        }

        static::addGlobalScope('noEmpty', function (Builder $builder) {
            $builder->noEmpty();
        });
    }
    
    public function clearGlobalScopes()
    {
        static::$globalScopes = [];
    }
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_name',
            ],
        ];
    }

    protected static function newFactory()
    {
        return \Database\Factories\Aimix\Shop\app\Models\ProductCategoryFactory::new();
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function products()
    {
        return $this->hasMany('Aimix\Shop\app\Models\Product');
    }

    public function childrenProducts()
    {
        return $this->hasManyThrough('Aimix\Shop\app\Models\Product', 'Aimix\Shop\app\Models\Category', 'parent_id');
    }
    
    
    public function attributes()
    {
        return $this->belongsToMany('Aimix\Shop\app\Models\Attribute');
    }

    public function parent()
    {
        return $this->belongsTo('Aimix\Shop\app\Models\Category', 'parent_id');
    }

    public function children()
    {
        return $this->hasMany('Aimix\Shop\app\Models\Category', 'parent_id');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeNoEmpty($query){
      
      return $query->has('products')->orHas('childrenProducts');
    }

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getSlugOrNameAttribute()
    {
        if ($this->slug != '') {
            return $this->slug;
        }

        return $this->name;
    }

    public function getProductsAttribute()
    {
        if($this->parent_id)
            return $this->products()->get();
        else
            return $this->childrenProducts;
    }

    public function getLinkAttribute()
    {
        return url('catalog/' . $this->slug);
    }

    public function getInnerLinkAttribute()
    {
        return 'catalog/' . $this->slug;
    }

    public function getPricesAttribute()
    {
        return [
            'min' => $this->products->min('price'),
            'max' => $this->products->max('price')
        ];
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
