<?php

namespace Aimix\Shop\app\Models;

use Illuminate\Database\Eloquent\Builder;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use CrudTrait;
    use Sluggable;
    use SluggableScopeHelpers;
    use HasFactory;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'products';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];
    protected $casts = [
      'extras' => 'array'
    ];
    protected $fakeColumns = [
      'sales'
    ];
    
    public $modifications_array = [];
    
    public $isModificationRelation = false;
    public $test = [];

    public $images_array = [];
    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    protected static function boot()
    {
        parent::boot();
        if(config('aimix.aimix.enable_languages')) {
          $language = session()->has('lang')? session()->get('lang'): 'ru';
          
          static::addGlobalScope('language', function (Builder $builder) use ($language) {
              $builder->where('language_abbr', $language);
          });
        }

        static::addGlobalScope('active', function (Builder $builder) {
          $builder->active();
      });
    }
    
    public function clearGlobalScopes()
    {
        static::$globalScopes = [];
    }
    
    public function toArray()
    {
      return [
        'id' => $this->id,
        'name' => $this->name,
        'slug' => $this->slug,
        'category_id' => $this->category_id,
        'category_name' => $this->category->name,
        'brand_id' => $this->brand_id,
        'price' => $this->baseModification->price,
        'old_price' => $this->baseModification->old_price,
        'sale_percent' => $this->salePercent,
        'is_active' => $this->is_active,
        'is_hit' => $this->is_hit,
        'is_new' => $this->is_new,
        'is_recommended' => $this->is_recommended,
        'rating' => $this->rating,
        'attrs' => $this->baseModification->getPluckedAttributesArray(),
        'extras' => $this->extras,
        'image' => url($this->image),
        'images' => $this->baseModification->images,
        'link' => $this->link,
        'amount' => isset($this->amount)? $this->amount : 1,
        'code' => $this->code,
        'in_stock' => $this->baseModification->in_stock,
        'description' => nl2br($this->description),
        'discount_amount' => $this->discountAmount
        
      ];
    }
    

    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'slug_or_name',
            ],
        ];
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function modifications()
    {
      $this->isModificationRelation = true;
      return $this->hasMany('Aimix\Shop\app\Models\Modification');
    }
    
    public function category()
    {
      return $this->belongsTo('\Aimix\Shop\app\Models\Category', 'category_id');
    }
    
    public function brand()
    {
      return $this->belongsTo('\Aimix\Shop\app\Models\Brand');
    }

    public function reviews()
    {
      return $this->hasMany('\Aimix\Review\app\Models\Review');
    }
    
    public function orders()
    {
      return $this->belongsToMany('\Aimix\Shop\app\Models\Order');
    }
    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */
    public function scopeActive($query)
    {
      return $query->where('products.is_active', 1);
    }
    
    public function scopeHits($query)
    {
      return $query->where('products.is_hit', 1);
    }
    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getSlugOrNameAttribute()
    {
        if ($this->slug != '') {
            return $this->slug;
        }
        return $this->name;
    }
    
    public function getSalesAttribute()
    {
      return json_decode($this->extras['sales']);
    }
    
    public function getComplectationsAttribute()
    {
      return $this->modifications()->base()->extras['complectations'];
    }

    public function getNotBaseModificationsAttribute()
    {
      return $this->modifications()->notBase();
    }
    
    public function getBaseModificationAttribute()
    {
      return $this->modifications()->base();
    }
    
    public function getBaseAttributesAttribute()
    {
      return $this->baseModification->attrs()->important()->get();
    }
    public function getFullnameAttribute()
    {
      return $this->brand->name . ' ' . $this->name;
    }
    
    public function getPriceAttribute()
    {
      
      $price = $this->baseModification->price;
      $old_price = $this->baseModification->old_price;
      
      // foreach($this->notBaseModifications->get() as $mod) {
      //   if($mod->price && (!$price || $mod->price < $price)) {
      //     $price = $mod->price;
      //     $old_price = $mod->old_price;
      //   }
      // }
      
      return $price;
    }
    
    public function getOldPriceAttribute()
    {
      $price = $this->baseModification->price;
      $old_price = $this->baseModification->old_price;
      
      // foreach($this->notBaseModifications->get() as $mod) {
      //   if($mod->price && (!$price || $mod->price < $price)) {
      //     $price = $mod->price;
      //     $old_price = $mod->old_price;
      //   }
      // }
      
      return $old_price;
    }
    
    public function getLinkAttribute()
    {
      $category_slug = $this->category->slug;
      
      return url('/catalog/' . $category_slug . '/' . $this->slug);
    }

    
    public function getInnerLinkAttribute()
    {
      $category_slug = $this->category->slug;
      
      return 'catalog/' . $category_slug . '/' . $this->slug;
    }
    
    public function getImagesAttribute()
    {
      return $this->modifications->where('is_default', 1)->first()->images;
    }

    public function getSalePercentAttribute()
    {
      return $this->baseModification->first() && $this->baseModification->old_price ? number_format(($this->baseModification->old_price - $this->baseModification->price) * 100 / $this->baseModification->old_price, 0) : null;
    }

    public function getDiscountAmountAttribute()
    {
      return $this->old_price? $this->old_price - $this->price : null;
    }

    public function getCodeAttribute()
    {
      return $this->baseModification->code;
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
    public function setModAttribute($value)
    {
      $this->modifications_array = $value;
    }
    
    public function setImagesAttribute($value)
    {
      $this->images_array = $value;
    }
}
