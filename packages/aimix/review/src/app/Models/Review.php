<?php

namespace Aimix\Review\app\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Review extends Model
{
    use CrudTrait;
    use HasFactory;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'reviews';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];
    // protected $dates = [];

    protected $casts = [
      'images' => 'array'
    ];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */
    protected static function boot()
    {
        parent::boot();
          static::addGlobalScope('moderated', function (Builder $builder) {
              $builder->where('is_moderated', 1);
          });
    }
    
    public function clearGlobalScopes()
    {
        static::$globalScopes = [];
    }

    public function toArray()
    {
      return [
        "id" => $this->id,
        "is_moderated" => $this->is_moderated,
        "type" => $this->type,
        "name" => $this->name,
        "email" => $this->email,
        "category" => $this->category,
        "product_id" => $this->product_id,
        "product_link" => $this->product ? $this->product->link : null,
        "rating" => $this->rating,
        "file" => $this->file,
        "images" => $this->images? $this->imagesArray : null,
        "text" => $this->text,
        "created_at" => \Carbon\Carbon::createFromTimeStamp(strtotime($this->created_at))->diffForHumans(),
      ];
    }
    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
    public function product()
    {
      return $this->belongsTo('\Aimix\Shop\app\Models\Product');
    }
    
    public function transaction() {
      return $this->hasOne('Aimix\Account\app\Models\Transaction');
    }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getImagesArrayAttribute()
    {
      return array_map(function($img) {
        return url($img);
      }, $this->images);
    }
    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
