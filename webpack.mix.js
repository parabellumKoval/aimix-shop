const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .less('resources/less/index/index.less','public/css/index.css')
    .less('resources/less/catalog/mainCatalog.less','public/css/mainCatalog.css')
    .less('resources/less/product/mainProduct.less','public/css/mainProduct.css')
    .less('resources/less/reviews/reviews.less','public/css/reviews.css')
    .less('resources/less/blog-page/mainBlog.less','public/css/mainBlog.css')
    .less('resources/less/article/mainArticle.less','public/css/mainArticle.css')
    .less('resources/less/comparison/mainComparison.less','public/css/mainComparison.css')
    .less('resources/less/cart/mainCart.less','public/css/mainCart.css')
    .less('resources/less/checkout/mainCheckout.less','public/css/mainCheckout.css')
    .less('resources/less/faq/mainFaq.less','public/css/mainFaq.css')
    .less('resources/less/contact/mainContact.less','public/css/mainContact.css')
    .less('resources/less/promo-code/mainPromoCode.less','public/css/mainPromoCode.css')
    .less('resources/less/profile/mainProfile.less','public/css/mainProfile.css')
    .js('packages/aimix/shop/src/resources/js/fields/modification.js', 'packages/aimix/shop/js/fields')
    .js('packages/aimix/shop/src/resources/js/fields/product_images.js', 'packages/aimix/shop/js/fields')
    .js('packages/aimix/gallery/src/resources/js/fields/gallery_images.js', 'packages/aimix/gallery/js/fields')