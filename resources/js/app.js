require('./bootstrap');

import VueRouter from 'vue-router'
import VueLazyload from 'vue-lazyload'
    
Vue.use(VueLazyload,{
  preLoad: 1,
//   error: 'dist/error.png',
  loading: '/video/pre.png',
  attempt: 1
})

Vue.use(VueRouter)

import home from './components/pages/homePage.vue'
import catalog from './components/pages/catalog.vue'
import product from './components/pages/product.vue'
import reviews from './components/pages/reviews.vue'
import blog from './components/pages/blog.vue'
import article from './components/pages/article.vue'
import comparison from './components/pages/comparison.vue'
import cart from './components/pages/cart.vue'
import checkout from './components/pages/checkout.vue'
import order from './components/pages/order.vue'
import faq from './components/pages/faq.vue'
import contact from './components/pages/contact.vue'
import profile from './components/pages/profile.vue'
import promoCode from './components/pages/promoCode.vue'
import footer from './components/includes/footer.vue'
import header from './components/includes/header.vue'
import modals from './components/includes/modals.vue'

import favoritesMix from './mixins/favoritesMix.js'


// const home = () => import(/* webpackChunkName: "home" */ './components/pages/homePage.vue');
// const clients = () => import(/* webpackChunkName: "clients" */ './components/pages/clients.vue');
// const abourUsPage = () => import(/* webpackChunkName: "abourUsPage" */ './components/pages/about-us-page.vue');
// const contact = () => import(/* webpackChunkName: "contact" */ './components/pages/contact.vue');
// const events = () => import(/* webpackChunkName: "events" */ './components/pages/events.vue');
// const eventPage = () => import(/* webpackChunkName: "eventPage" */ './components/pages/event-page.vue');
// const blog = () => import(/* webpackChunkName: "blog" */ './components/pages/blog.vue');
// const blogPage = () => import(/* webpackChunkName: "blogPage" */ './components/pages/blog-page.vue');
// const faq = () => import(/* webpackChunkName: "faq" */ './components/pages/faq.vue');
// const gallery = () => import(/* webpackChunkName: "gallery" */ './components/pages/gallery.vue');
// const corporatives = () => import(/* webpackChunkName: "corporatives" */ './components/pages/corporatives.vue');
// const partnership = () => import(/* webpackChunkName: "partnership" */ './components/pages/partnership.vue');
// const reviewsPage = () => import(/* webpackChunkName: "reviewsPage" */ './components/pages/reviews-page.vue');
// const planEvents = () => import(/* webpackChunkName: "planEvents" */ './components/pages/plan-events.vue');

import {mainScripts} from './main.js'



// 2. Определяем несколько маршрутов
// Каждый маршрут должен указывать на компонент.
// "Компонентом" может быть как конструктор компонента, созданный
// через `Vue.extend()`, так и просто объект с опциями компонента.
// Мы поговорим о вложенных маршрутах позднее.

const mainRoutes = [
    { path: '/', name:"Home", component: home },
    { path: '/catalog/:category', name: "catalog", component: catalog,
        meta: {
            breadcrumb: [
                { name: 'Каталог', link: "/catalog" }
            ]
        }
    },
     { path: '/catalog/:category/:slug', name: 'product', component: product, sub: true, props: true,
        meta: {
            breadcrumb: [
                { relatives: true},
                { name: 'Каталог', link: "/catalog" },
            ]
        }
    },
    { path: '/reviews', name: "reviews", component: reviews,
        meta: {
            breadcrumb: [
                { name: 'Отзывы покупателей о нашем магазине', link: "/reviews" }
            ]
        }
    },
    { path: '/blog/:category?', name:"blog", component: blog,
        meta: {
            breadcrumb: [
                { name: 'Блог', link: "/blog" },
            ],
        }
    },
    { path: '/blog/:category/:slug', name: 'article', component: article, sub: true, props: true,
        meta: {
            breadcrumb: [
                { relatives: true},
                { name: 'Блог', link: "/blog" },
            ],
        }
    },
    { path: '/comparison', name:"comparison", component: comparison, elseMenu: true,
        meta: {
            breadcrumb: [
                { name: 'Сравнение товаров', link: "/comparison" }
            ]
        }
    },

    { path: '/cart', name:"Корзина", component: cart,
         meta: {
            breadcrumb: [
                { name: 'Корзина', link: "/car" }
            ]
        }
    },
    { path: '/checkout', name:"checkout", component: checkout,
        meta: {
            breadcrumb: [
                { name: 'Оформление заказа', link: "/checkout" }
            ]
        }
    },
    { path: '/order', name:"order", component: order,
        meta: {
            breadcrumb: [
                { name: 'Оформление заказа', link: "/order" }
            ]
        }
    },
    { path: '/faq', name:"FAQ", component: faq, elseMenu: true,
        meta: {
            breadcrumb: [
                { name: 'FAQ', link: "/faq" }
            ]
        }
    },

    { path: '/contact', name:"Контакты", component: contact, elseMenu: true,
        meta: {
            breadcrumb: [
                { name: 'Контакты', link: "/contact" }
            ]
        }
    },
    { path: '/profile', name:"Мой профиль", component: profile, sub: true,
        meta: {
            breadcrumb: [
                { name: 'Мой профиль', link: "/reviews" }
            ]
        }
    },
    { path: '/promo-code', name:"Промокоды", component: promoCode, sub: true,
        meta: {
            breadcrumb: [
                { name: 'Промокоды', link: "/promo-code" }
            ]
        }
    },
//     { path: '/policy', name:"Политика конфиденциальности", component: policy, sub: true},
    { path: '*',redirect: '/', sub: true}
];


const router = new VueRouter({
    mode: 'history',
    routes: mainRoutes,
    linkExactActiveClass: 'active'
});

const app = new Vue({
    el: '#app',
    data: {
        windowWidth: 0,
        productDetails: null
    },
    mixins: [favoritesMix],
    components: {
        mainFooter: footer,
        mainHeader: header,
        modals: modals
    },
    methods: {
        updateJs() {
            mainScripts();
        },
        scrollToTop() {
            setTimeout(() => {
                window.scrollTo({
                    top: 0,
                });
            }, 5);
        },
    },
    mounted () {
        // Set window width
        
        this.windowWidth = window.innerWidth;
        window.addEventListener('resize', () => {
            this.windowWidth = window.innerWidth;
        });
        
    },
    updated() {
        setTimeout(() =>{
            this.updateJs();
        }, 2000);
        this.scrollToTop();
    },
    router,
});

