var favoritesMix = {
  data: function () {
    return {
      favorites: null,
      favoriteProducts: [],
      viewed: null,
      viewedProducts: [],
      favTab: 'viewed',
    }
  },
  computed: {
    totalFavorites: function() {
      return this.favorites.length;
    },
    favoritesOrViewed: function() {
      return this.favTab === 'viewed'? this.viewedProducts : this.favoriteProducts;
    }
  },
  methods: {
    addToFavorites: function(item) {
      let id = item.id;

      if(this.favorites.includes(id)) {
        this.favorites.splice(this.favorites.indexOf(id), 1);
        noty('success', 'Удалено');
        return;
      }

      this.favorites.push(id);
      noty('success', 'Добавлено');
    },
    validateFav: function(event) {
      if(this.totalFavorites === 0) {
        noty('error', 'В избранном пусто');
        return event.preventDefault();
      }
    },
    getFavorites: function() {
      let component = this;
      axios.post('/api/products/byIds', {ids: this.favorites})
           .then(function(response) {
              component.favoriteProducts = response.data.data;
              console.log('fav', component.favoriteProducts);
           });
    }
  },
  watch: {
    favorites: {
      handler: function(value) {
        localStorage[appName + '_fav'] = JSON.stringify(this.favorites);
        this.getFavorites();
      },
      deep: true
    },
  },
  created: function() {
    console.log('favorites mixin');
    let fav = JSON.parse(localStorage.getItem(appName + '_fav'));
    this.$root.$on('add-to-favorites', this.addToFavorites);
    
    if(!fav)
      fav = [];

    this.favorites = fav;
  }
}

export default favoritesMix;