<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Template</title>
    <meta name="description" content="Template">
	<!-- <link href="../fonts/Circe-Regular.ttf" rel="preload" as="font" type="font/ttf" crossorigin>
	<link href="../fonts/Circe-Light.ttf" rel="preload" as="font" type="font/ttf" crossorigin>
	<link href="../fonts/Circe-Bold.ttf" rel="preload" as="font" type="font/ttf" crossorigin>
	<link href="../fonts/Circe-ExtraBold.ttf" rel="preload" as="font" type="font/ttf" crossorigin>
     -->
    <link rel="stylesheet" href="" data-href="{{ url('/css/') }}">
    <link href="{{ url('/packages/noty/noty.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app" v-cloak>
        <main-header></main-header>

        <section class="header__burger-wrapper js-popup" data-target="burger" v-if="windowWidth < 768">
            <div class="header__burger-top">
                <div class="change-lang">
                    <label class="change-lang__wrapper">
                        <input type="checkbox" class='hidden-input'>
                        <span class="change-lang-decor-elem"></span>
                        <span class="change-lang__item">Ru</span>
                        <span class="change-lang__item">En</span>
                    </label>
                </div>
                <button class="close__popup close__popup--header js-close">
                    <span class="icon-close"></span>
                </button>
            </div>
            <button class="header__burger-search">
                <span class="text">Поиск</span>
                <span class="icon-search input-search-icon"></span>
            </button>
            <div class="header__burger-catalog">
                <button class="header__burger-catalog__button">
                    <span class="text">Каталог</span>
                    <span class="icon-drop-arrow"></span>
                </button>
            </div>
            <div class="header__burger-items__wrapper">
                <div class="header__burger-item">
                    <ul class="header__burger-item__list">
                        <li class="header__burger-item__item">
                            <a href="#">Доставка и оплата</a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">Возврат и обмен</a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">Сравнение <span>(1)</span></a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">Контакты</a>
                        </li>
                        <li class="header__burger-item__item active">
                            <a href="#">FAQ</a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">Условия использования сайта</a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">О компании</a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">Блог</a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">Промокоды на сентябрь 2020</a>
                        </li>
                    </ul>
                </div>
                <div class="header__burger-item">
                    <ul class="header__burger-item__list">
                        <li class="header__burger-item__item">
                            <a href="#">Мой заказы</a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">Мои возвраты</a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">Мой профиль</a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">Мои отзывы</a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">Избранное</a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">Просмотренное</a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">Мои промокоды</a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">Настройки о рассылке</a>
                        </li>
                    </ul>
                </div>
                <div class="header__burger-item">
                    <p class="header__burger-item__caption">Нужна помощь?</p>
                    <ul class="header__burger-item__list">
                        <li class="header__burger-item__item">
                            <a href="#">+38 (097) 234 54 65</a>
                        </li>
                        <li class="header__burger-item__item">
                            <a href="#">+38 (097) 234 54 65</a>
                        </li>
                        <li class="header__burger-item__item">
                            <button class="js-button" data-target="help">Перезвоните мне</button>
                        </li>
                    </ul>
                </div>
                <div class="header__burger-item">
                    <p class="header__burger-item__caption">Магазины</p>
                    <ul class="header__burger-item__list">
                        <li class="header__burger-item__item">
                            <a href="#">г. Харьков, ул. Пушкинская, 45 </a>
                        </li>
                    </ul>
                </div>
            </div>
        </section>

        <transition name="moveInUp">
            <router-view :window-width="windowWidth" @add-to-favorites="addToFavorites"></router-view>
        </transition>

        <main-footer></main-footer>
        <modals></modals>
    </div>
    <script src="{{ url('/packages/noty/noty.js') }}" type="text/javascript"></script>
    <script>
        var appName = "{{ $app_name }}";

        function noty(type, message){
            new Noty({
                type: type,
                theme: 'push_notification__item',
                text: '<p>' + message + '</p><span class="icon-close"></span>',
                timeout: 5000,
                container: '.push_notification__list',
                
            }).show();
        }
    </script>
    <script src="{{ url('/js/app.js') }}"></script>
</body>
</html>