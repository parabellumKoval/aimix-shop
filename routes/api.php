<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Banners
Route::apiResource('banners', 'API\BannerController');

// Product categories
Route::get('product_categories/popular', 'API\ProductCategoryController@popular');
Route::apiResource('product_categories', 'API\ProductCategoryController');

// Products
Route::get('products/popular', 'API\ProductController@popular');
Route::post('products/byIds', 'API\ProductController@byIds');
Route::post('products/byCategory', 'API\ProductController@byCategory');
Route::post('products/filtered', 'API\ProductController@filtered');
Route::apiResource('products', 'API\ProductController');

// Reviews
Route::get('reviews/paginate', 'API\ReviewController@paginate');
Route::apiResource('reviews', 'API\ReviewController');

// Articles
Route::get('articles/paginate', 'API\ArticleController@paginate');
Route::apiResource('articles', 'API\ArticleController');

// Categories
Route::apiResource('categories', 'API\CategoryController');

// Faq
Route::apiResource('faqs', 'API\FaqController');

// Faq categories
Route::apiResource('faq_categories', 'API\FaqCategoryController');

// Attributes
Route::apiResource('attributes', 'API\AttributeController');
Route::get('attributes/byCategory/{id}', 'API\AttributeController@byCategory');

// Brands
Route::apiResource('brands', 'API\BrandController');
Route::get('brands/byCategory/{id}', 'API\BrandController@byCategory');