<?php

use Illuminate\Support\Facades\Route;
use App\Models\Article;
use Aimix\Banner\app\Models\Banner;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', function() {
  $banner = Banner::factory()->make();

  dd($banner);
  // dd(Telegram::getUpdates());
  // $array = [900, 1800, 750, 2100, 2450, 2900, 3400, 3650];
  // Aimix\Shop\app\Models\Order::create([
  //   'code' => \Carbon\Carbon::now()->timestamp,
  //   'price' => $array[array_rand($array)],
  //   'info' => 'test'
  // ]);
});

Route::get('/{any}', 'SpaController@index')->where('any', '.*');

// Route::post('/<token>/webhook', function () {
//   $updates = Telegram::getWebhookUpdates();

//   return 'ok';
// });
