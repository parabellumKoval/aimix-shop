<?php

return [
  'enable_review_type' => false,
  'enable_review_for_product' => false,
  'enable_rating' => true,
  'per_page' => 3,

];
