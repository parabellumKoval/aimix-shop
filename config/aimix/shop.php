<?php

return [
    'is_modifications_category' => true,
    'currency_default' => 'usd',
    'enable_in_stock' => false,
    'enable_in_stock_count' => false, // true = numeric, false = boolean
    'enable_modifications' => false, // false = only base modification
    'enable_multiple_product_images' => true, // false = one image per product
    'enable_complectations' => false,
    'enable_product_promotions' => false,
    'enable_is_hit' => true, // enable "Hit" products
    'enable_is_new' => false, // enable "New" products
    'enable_is_recommended' => false, // enable "Recommended" products
    'enable_brands' => true,
    'enable_attribute_groups' => false,
    'enable_attribute_icon' => false,
    'enable_product_rating' => false,
    'enable_product_category_pages' => false,
    'modifications_in_order' => false, // false = products in order
    'per_page' => 2
];
